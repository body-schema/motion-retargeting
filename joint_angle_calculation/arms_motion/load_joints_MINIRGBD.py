import numpy as np
import arm_joint_angles as aja
import speed_and_acceleration as sa

import sys
import copy
import csv
from os import listdir, mkdir, path
from os.path import join

global joints
joints = ['global_',
          'left_hip',
          'right_hip',
          'mid_hip',
          'left_knee',
          'right_knee',
          'mid_hip1',
          'left_ankle',
          'right_ankle',
          'mid_hip2',
          'left_toes',
          'right_toes',
          'neck',
          'left_shoulder',
          'right_shoulder',
          'head',
          'left_upper_arm',
          'right_upper_arm',
          'left_elbow',
          'right_elbow',
          'left_wrist',
          'right_wrist',
          'left_fingers',
          'right_fingers',
          'nose_vertex']

CROP = False
MIMO = True

def load_joints(address):
    '''
    :param address: the adress of text file with joints
    :return: dictionary with xyz coordinates of the joint
    '''
    f = open(address, "r")
    result = {}
    for line in f:
        my_line = line.split()
        for i in range(4):
            if i < 3:
                my_line[i] = float(my_line[i])
            else:
                global joints
                result[joints[int(my_line[i])]] = np.array(my_line[0:3])
    return result

def numbers(x):
    '''
    function to determine key of sorting files of MINI_RGBD dataset
    :param x: the name of a file
    :return: digits acoording to which the sorting should be done
    '''
    return x[-9:-4]



def main():
    address = sys.argv[1]

    if not path.exists('data'):
        mkdir('data')

    f = open('data/joint_angles.csv', 'w', newline='')
    #sp = open('data/speeds.csv', 'w', newline='')
    #acc = open('data/accelerations.csv', 'w', newline='')

    writer1 = csv.writer(f)
    #writer2 = csv.writer(sp)
    #writer3 = csv.writer(acc)
    header = ['Shoulder pitch', 'Shoulder roll', 'Shoulder yaw', 'Elbow angle',
              'Wrist pronosupination', 'Wrist pitch', 'Wrist yaw']
              # 'Hand finger', 'Thumb opposition', 'Thumb proximal',
              #'Thumb distal', 'Index proximal', 'Index distal', 'Middle proximal', 'Middle distal', 'Ring and little finger flexion']

    writer1.writerow(header)
    #writer2.writerow(header)
    #writer3.writerow(header)

    ld = listdir(address)
    sorted_files = sorted(ld, key=numbers)

    old_an_la = [0 for i in range(16)]
    old_an_ra = [0 for i in range(16)]
    old_sp_la = [0 for i in range(16)]
    old_sp_ra = [0 for i in range(16)]

    counter = 0
    for file in sorted_files:
        joints = load_joints(join(address, file))
        left_arm = aja.Arm(joints, True, CROP,MIMO)
        base = [counter, 'left']
        row = left_arm.angles[0:len(header)]
        writer1.writerow(row)
        
        #speeds = sa.calculate_speed(old_an_la, left_arm.angles)
        #old_an_la = left_arm.angles
        #row = speeds[0:len(header)]
        #writer2.writerow(row)
#
        #accelerations = sa.calculate_acceleration(old_sp_la, speeds)
        #old_sp_la = speeds
        #row = accelerations[0:len(header)]
        #writer3.writerow(row)

        base = ['', 'right']
        rightArm = aja.Arm(joints, False, CROP, MIMO)
        row = rightArm.angles[0:len(header)]
        writer1.writerow(row)

        #speeds = sa.calculate_speed(old_an_ra, rightArm.angles)
        #old_an_ra = rightArm.angles
        #row = speeds[0:len(header)]
        #writer2.writerow(row)
#
        #accelerations = sa.calculate_acceleration(old_sp_ra, speeds)
        #old_sp_ra = speeds
        #row = accelerations[0:len(header)]
        #writer3.writerow(row)
        print(f'Left arm: {left_arm.angles[0:len(header)]}')
        print(f'Right arm: {rightArm.angles[0:len(header)]}')
        counter += 1
        break
    f.close()
    #sp.close()
    #acc.close()


if __name__ == '__main__':
    main()