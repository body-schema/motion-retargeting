from xml.etree.ElementTree import QName
import numpy as np
import math_utils as mu
import leg_kin as l_kin
import rotations_translations as rt

class Leg():
    def __init__(self, joints, left, crop_icub=True, csv=True) -> None:
        self.joints = joints = joints
        self.left = left
        self.crop_icub = crop_icub
        self.csv = csv
        # joints
        self.hip = None
        self.knee = None
        self.ankle = None
        #vectors
        self.vec_low_spine = None       # middle hip to neck
        self.vec_thigh = None       # side hip to knee
        self.vec_calf = None        # knee to ankle
        self.vec_hip = None         # one side hip to other side hip
        self.ref_roll = None        # vector for roll reference
        self.init_vectors()
        #angles
        self.angles = [0 for i in range(6)] # hip pitch, roll, yaw, knee, ankle pitch, roll
        self.count_angles()

    def init_vectors(self):
        '''
        function to initialize all the vectors needed
        :return: None
        '''
        self.vec_hip = np.subtract(self.joints['left_hip'], self.joints['right_hip'])
        middle_hip = np.add(self.joints['left_hip'], self.joints['right_hip'])/2
        self.vec_low_spine = np.subtract(middle_hip, self.joints['mid_hip'])
        if self.left:
            self.hip = self.joints['left_hip']
            self.knee = self.joints['left_knee']
            self.ankle = self.joints['left_ankle']
        else:
            self.hip = self.joints['right_hip']
            self.knee = self.joints['right_knee']
            self.ankle = self.joints['right_ankle']
            self.vec_hip *= -1
        self.vec_thigh = np.subtract(self.knee, self.hip)
        self.vec_calf = np.subtract(self.ankle, self.knee)
        return None
    
    def knee_angle(self):
        '''
        function to count knee angle
        :return: angle in knee in degrees
        '''
        self.angles[3] = np.rad2deg(mu.count_angle_unsigned(self.vec_thigh, self.vec_calf))
        if self.crop_icub:
            self.angles[3] = mu.crop_to_interval(self.angles[3], 100, 0)
        return 180 - self.angles[3]

    def hip_pitch(self):
        '''
        function to calculate hip pitch
        :return: hip pitch in degrees
        '''
        # set the coordinate frame to the hip joint
        hip_turn = np.eye(4,4)
        hip_turn[0:3, 3] = self.hip
        if self.left:
            base = mu.axis_angle_rotation_mat(np.array([0, 1, 0]), self.vec_hip)
        else:
            base = mu.axis_angle_rotation_mat(np.array([0, 1, 0]), -self.vec_hip)
        R2R = mu.axis_angle_rotation_mat(base[0:3, 0], self.vec_low_spine)
        base = hip_turn @ R2R @ base
        if self.left:
            base = base @ rt.Rx(np.pi/2)
        else:
            base = base @ rt.Rx(-np.pi/2)

        # project onto the low spine vector and calculate the hip pitch
        norm = base[0:3,2]
        projected1 = mu.project_vect_to_plane(self.vec_low_spine, norm)
        projected2 = mu.project_vect_to_plane(self.vec_thigh, norm)
        if self.left:
            pitch = mu.count_angle_signed(projected1, projected2, norm)
        else:
            pitch = mu.count_angle_signed(projected1, projected2, -norm)

        self.angles[0] = np.rad2deg(pitch)
        self.ref_roll = projected2
        if self.crop_icub:
            self.angles[0] = mu.crop_to_interval(self.angles[0], 92, -30)

        return self.angles[0]
        
    def hip_roll(self):
        '''
        function to calculate hip roll
        :return: hip roll in degrees
        '''
        self.angles[1] = np.rad2deg(mu.count_angle_unsigned(self.ref_roll, self.vec_thigh))

        # determining sign
        ref = mu.project_vect_to_plane(self.ref_roll, self.vec_low_spine)
        vec = mu.project_vect_to_plane(self.vec_thigh, self.vec_low_spine)

        if self.left:
            sign = mu.count_angle_signed(ref, vec, -self.vec_low_spine)
        else:
            sign = mu.count_angle_signed(ref, vec, self.vec_low_spine)
        sign = sign / abs(sign)

        # aplicating sign
        self.angles[1] *= sign
        if self.angles[0] < 0:
            self.angles[1] *= -1
    
        return self.angles[1]
    
    def hip_yaw_agnostic(self):
        '''
        function to calculate hip yaw
        :return: hip roll in degrees
        '''
        # set the coordinate system to the hip joint
        hip_turn = np.eye(4,4)
        hip_turn[0:3, 3] = self.hip
        
        if self.left:
            rot = self.vec_hip
        else:
            rot = -self.vec_hip
        R = mu.axis_angle_rotation_mat(np.array([0, 1, 0]), rot)
        base = R
        R2 = mu.axis_angle_rotation_mat(base[0:3, 0], self.vec_low_spine)
        base = hip_turn @ R2 @ base
        if not self.left:
            base = base @ rt.Rx(-np.pi/2)
        else:
            base = base @ rt.Rx(np.pi/2)
        
        #apply pitch
        norm = base[0:3, 2]
        theta = np.deg2rad(self.angles[0])
        if not self.left:
            theta = -theta
        base = base @ rt.Rz(theta) @ rt.Rx(np.pi/2)

        # apply roll
        norm = base[0:3, 2]

        theta = np.deg2rad(-self.angles[1])
        roll = theta
        base = base @ rt.Rz(roll) @ rt.Tx(np.linalg.norm(self.vec_thigh))

        # now proceed to yaw calculation
        mid_base = base @ rt.Rz(np.deg2rad(self.angles[3])) @ rt.Tx(np.linalg.norm(self.vec_calf))
        # zero--yaw vector
        without_rot = np.subtract(mid_base[0:3, 3], base[0:3, 3])
        norm = base[0:3,0]
        
        # now calculate the yaw
        proj1 = mu.project_vect_to_plane(without_rot, norm)
        proj2 = mu.project_vect_to_plane(self.vec_calf, norm)
        if self.left:
            yaw = mu.count_angle_signed(proj1, proj2, -norm)
        else:
            yaw = mu.count_angle_signed(proj1, proj2, norm)
        self.angles[2] = np.rad2deg(yaw)
        return self.angles[2]

    def count_angles(self):
        self.hip_pitch()
        self.hip_roll()
        self.knee_angle()
        self.hip_yaw_agnostic()