import matplotlib.pyplot as plt
import numpy as np
import sys


FROM = 0
TO = 1000

FRAMERATE = 1
FONTSIZE = 30
FONTSIZE2 = 20
LINEWIDTH = 2

def load_leg_joint_angles(file):
    left_leg = [[] for i in range(5)]
    right_leg = [[] for i in range(5)]
    left = True
    first = True
    for line in file:
        if first:
            first = False
            continue
        my_line = line.split(',')
        if left:
            for j in range(4):
                left_leg[j].append(float(my_line[j]))
        else:
            for j in range(4):
                right_leg[j].append(float(my_line[j]))
        left = not left

    return left_leg, right_leg

def load_angles_from_matlab(file):
    left = []
    right = []
    left_bool = True
    for line in file:
        my_line = line.split(',')
        print(len(my_line))
        if left_bool:    
            left = [float(i) for i in my_line]
        else:
            right = [float(i) for i in my_line]
        left_bool = not left_bool
    return left, right

def plot(arm, ylab, name, pitches = None, rolls = None, yaws = None, knees = None):
    time = [i/FRAMERATE for i in range(TO-FROM)]
    fig, _ = plt.subplots(4, 1, figsize=(3840/1.5/96, 2160/96))
    fig.suptitle(ylab + ' leg joint angles', fontsize = 50)

    #min_border = [-30, 0, -70, -100]
    #max_border = [85, 85, 70, 0]
    min_border = [-133, -51, -32, -145]
    max_border = [20, 17, 41, 4]
    ylabels = ['hip pitch', 'hip roll', 'hip yaw', 'knee angle']


    for i in range(4):
        plt.subplot(4, 1, i+1)
        if i == 0 or i == 1:
            nas = -1
            konst = 0
        else:
            nas = 1
            konst = 0
        plt.plot(time, [(nas*k + konst) for k in arm[i][FROM:TO]], label = 'calculated', linewidth=LINEWIDTH)
        plt.plot(time, [min_border[i] for j in range(TO-FROM)], 'r--', label = 'iCub limits')
        plt.plot(time, [max_border[i] for j in range(TO-FROM)], 'r--')
        if i == 1:
            plt.plot(time, [90 for j in range(TO-FROM)], 'g--', label = 'gimbal lock')
        
        if i == 0 and pitches is not None:
            plt.plot(time, pitches[FROM:TO], label = 'matlab', linewidth = LINEWIDTH, linestyle = 'dashed')
        elif i == 2 and yaws is not None:
            plt.plot(time, yaws[FROM:TO], label = 'matlab', linewidth = LINEWIDTH, linestyle = 'dashed')
        plt.xticks(fontsize=FONTSIZE2)
        plt.yticks(fontsize=FONTSIZE2)

        plt.xlim(0, time[-1])
        plt.ylim(min(min(arm[i][FROM:TO]), min_border[i])-10, max(max(arm[i][FROM:TO]), max_border[i])+10)

        plt.ylabel(ylabels[i] + ' (°)', fontsize=FONTSIZE)
        plt.grid()
        plt.legend(fancybox=True, framealpha=0.5, fontsize=FONTSIZE2, bbox_to_anchor=(1.13, 1), loc ='upper right')

    plt.xlabel('Frame number', fontsize=FONTSIZE)
    plt.show()
    #fig.savefig(name + '.png')
    #fig.savefig(name + '.pdf', format='pdf', transparent=True)
    #fig.savefig(name + '.eps', format='eps', transparent=True)


def main():
    address = sys.argv[1]
    pitches = sys.argv[2]
    rolls = sys.argv[3]
    f_p = open(pitches, 'r')
    f_r = open(rolls, 'r')
    yaws = sys.argv[4]
    f_y = open(yaws, 'r')
    knees = sys.argv[5]
    f_k = open(knees, 'r')

    with open(address, 'r') as f:
        left_leg, right_leg = load_leg_joint_angles(f)
        l_pitch, r_pitch = load_angles_from_matlab(f_p)
        l_roll, r_roll = load_angles_from_matlab(f_r)
        l_yaw, r_yaw = load_angles_from_matlab(f_y)
        plot(left_leg, "Left", 'graphs/real_8w_left')#, l_pitch, l_roll, l_yaw)
        plot(right_leg, "Right", 'graphs/real_8w_right')#, r_pitch, r_roll, r_yaw)
    return None

if __name__ == '__main__':
    main()