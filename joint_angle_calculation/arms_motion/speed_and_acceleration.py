from hashlib import new
from mmap import ACCESS_DEFAULT
import numpy as np

TIME = 1/5

def calculate_speed(old_angles, new_angles):
    speed = []
    for i in range(len(old_angles)):
        if i > 6:
            speed.append(50)
            continue
        if old_angles[i] < 0 :
            old_an = old_angles[i] + 360
        else:
            old_an = old_angles[i]
        
        if new_angles[i] < 0 :
            new_an = new_angles[i] + 360
        else:
            new_an = new_angles[i]
        
        wanted_speed = (min(abs(old_an - new_an), 360 - abs(old_an - new_an)))/TIME
        speed.append(max(1, min(wanted_speed, 100)))
    return speed

def calculate_acceleration(old_speed, new_speed):
    acceleration = abs(np.subtract(old_speed,new_speed))/TIME
    return acceleration.tolist()


