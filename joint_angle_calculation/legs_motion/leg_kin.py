import numpy as np
from math import cos, sin

def evalDHMatrix(a, d, alph, thet):
    ar = [[cos(thet), -sin(thet)*cos(alph), sin(thet)*sin(alph), cos(thet) * a],
        [sin(thet), cos(thet)*cos(alph), -cos(thet)*sin(alph), sin(thet) * a],
        [0, sin(alph), cos(alph), d],
        [0, 0, 0, 1]]
    return np.matrix(ar)

def Rx(alpha):
    R = np.matrix([[1, 0, 0, 0], 
                    [0, cos(alpha), -sin(alpha), 0], 
                    [0, sin(alpha), cos(alpha), 0],
                    [0, 0, 0, 1]])
    return R



def count_left_leg(hPitch, hRoll, hYaw, Knee, aPitch, aRoll):
    LG_01 = evalDHMatrix(     0,       0,    -np.pi/2,      hPitch+np.pi/2);
    LG_12 = evalDHMatrix(     0,       0,    -np.pi/2,      hRoll+np.pi/2);
    LG_23 = evalDHMatrix(     0, -240.00,     np.pi/2,      hYaw-np.pi/2);
    LG_34 = evalDHMatrix(-220.0,       0,     np.pi  ,       Knee+np.pi/2);
    LG_45 = evalDHMatrix(     0,       0,     -np.pi/2,      aPitch);
    LG_56 = evalDHMatrix( -41.0,       0,        0,         aRoll);

    LG_sL0 = Rx(-np.pi/2)*np.eye(4)
    LG_sL0[0,3] = 0
    LG_sL0[1,3] = -68.1
    LG_sL0[2,3] = -119.9
    LG_sL1 = LG_sL0*LG_01;
    LG_sL2 = LG_sL1*LG_12;
    LG_sL3 = LG_sL2*LG_23;
    LG_sL4 = LG_sL3*LG_34;
    LG_sL5 = LG_sL4*LG_45;
    LG_sL6 = LG_sL5*LG_56;

    return LG_sL3[:, 3], LG_sL6[:, 3]

def count_right_leg(hPitch, hRoll, hYaw, Knee, aPitch, aRoll):
    RG_01 = evalDHMatrix(     0,        0,     np.pi/2,      hPitch + np.pi/2);
    RG_12 = evalDHMatrix(     0,        0,     np.pi/2,      hRoll + np.pi/2);
    RG_23 = evalDHMatrix(     0,   240.00,    -np.pi/2,      hYaw - np.pi/2);
    RG_34 = evalDHMatrix(-220.0,        0,     np.pi  ,      Knee + np.pi/2);
    RG_45 = evalDHMatrix(     0,        0,     np.pi/2,      aPitch);
    RG_56 = evalDHMatrix( -41.0,        0,     np.pi  ,      aRoll);

    RG_sL0 = Rx(-np.pi/2)*np.eye(4)
    RG_sL0[0,3] = 0
    RG_sL0[1,3] = 68.1
    RG_sL0[2,3] = -119.9

    RG_sL1 = RG_sL0*RG_01;
    RG_sL2 = RG_sL1*RG_12;
    RG_sL3 = RG_sL2*RG_23;
    RG_sL4 = RG_sL3*RG_34;
    RG_sL5 = RG_sL4*RG_45;
    RG_sL6 = RG_sL5*RG_56;

    return RG_sL3[0:3, 3], RG_sL6[0:3, 3]