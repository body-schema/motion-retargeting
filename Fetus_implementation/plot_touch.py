#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
this module was adopted from Dongmin Kim
and adapted to our purposes
"""

import cv2
import h5py
import numpy as np
import csv
import argparse
import pickle

import mujoco
from matplotlib.colors import LinearSegmentedColormap

import matplotlib.pyplot as plt
from PIL import Image

from mujoco_infant.envs.isi_env import ISIEnv
from mujoco_infant.utils import parse_initial_joint_values


DATA = "data_18w_mimo"

def load_data_header(file_name):
    ret = []
    with open(file_name, "r") as f:
        reader = csv.reader(f, delimiter="\t")
        for header in reader:
            ret.extend(header)
    return ret


### COLORS INITIALIZATION ###

# get colormap
cmap_name = "GnBu"
ncolors = 1024
color_array = plt.get_cmap(cmap_name)(range(ncolors))

# change alpha values
color_array[:, -1] = np.linspace(0, 1, ncolors)

# create a colormap object
cmap = LinearSegmentedColormap.from_list(name=cmap_name, colors=color_array)


def get_site_rgba(value):
    # Cmap
    ret = cmap(value)
    return ret


def execute(args):
    env = ISIEnv(
        model_path=args.model_path, initial_joint_qpos=args.initial_joint_values
    )

    # Dummy render to initialize renderer
    if args.headless:
        env.render(
            mode="rgb_array",
            render_width=args.render_width,
            render_height=args.render_height,
        )
    else:
        env.render()

    baby_names = ["baby1"]

    data_path = f"data_8w1"

    with open(f'{DATA}/touch_data.pkl', 'rb') as fp:
        tactile = pickle.load(fp)

    tactile_names = load_data_header(f"default/tactile_names.txt")

    # Clip & Normalize data
    clip_max = 0.001
    # range = 0 ~ 0.001
    tactile_clip = np.clip(tactile, a_min=0, a_max=clip_max)
    # range = 0 ~ 1
    tactile_clip = tactile_clip / clip_max

    # Find the index of the valid tactile sites

    valid_tactile_ids = []
    valid_only_display_site_ids = []
    for tactile_id, tactile_name in enumerate(tactile_names):
        baby_name = tactile_name.split("_")[0]
        only_display_site_name = f"{baby_name}_only_display_{tactile_name}"

        if only_display_site_name in env.site_names:
            only_display_site_id = mujoco.mj_name2id(
                env.model, mujoco.mjtObj.mjOBJ_SITE, only_display_site_name
            )
            valid_tactile_ids.append(tactile_id)
            valid_only_display_site_ids.append(only_display_site_id)

    # Change the color of tactile site
    for only_display_site_id in valid_only_display_site_ids:
        only_display_site_size = 0.005
        env.model.site_rgba[only_display_site_id] = get_site_rgba(1)
        env.model.site_size[only_display_site_id][0] = only_display_site_size
        env.model.site_size[only_display_site_id][1] = 0.001
        env.model.site_size[only_display_site_id][2] = 0

    # Disable shadow
    env.model.light_active[0] = 0

    # Disable bone&actuator display
    env.render_flags["vopt"]["geomgroup"][env.GEOM_GROUP_BONE] = 0
    env.render_flags["vopt"]["tendongroup"][:] = [0] * 6
    env.render_flags["vopt"]["actuatorgroup"][:] = [0] * 6

    # Disable transparency of the baby
    env.render_flags["flags"]["VIS_TRANSPARENT"] = 0
    skin_ids = np.where(env.model.geom_group == env.GEOM_GROUP_RIGID_SKIN)[0]
    for skin_id in skin_ids:
        env.model.geom_rgba[skin_id][3] = 1

    # Remove Floor
    if "env_etc_floor" in env.geom_names:
        env.model.geom("env_etc_floor").rgba[3] = 0

    # Set Camera position
    if "track" in env.camera_names:
        camera_id = env.camera_names.index("track")
        camera_track = env.model.camera("track")
        if args.camera_position == "front":
            camera_track.pos = [1, 0, 0]
            camera_track.quat = [0.5, 0.5, 0.5, 0.5]
        elif args.camera_position == "back":
            camera_track.pos = [-1, 0, 0]
            camera_track.quat = [0.5, 0.5, -0.5, -0.5]
        elif args.camera_position == "side":
            camera_track.pos = [0, -1, 0]
            camera_track.quat = [0.701, 0.701, 0, 0]

    # Let the baby fixed on the air
    exclude_joint_list = [
        "pelvis_tilt_AXIS_rotation1",
        "pelvis_list_AXIS_rotation2",
        "pelvis_rotation_AXIS_rotation3",
        "pelvis_tx_AXIS_translation1",
        "pelvis_ty_AXIS_translation2",
        "pelvis_tz_AXIS_translation3",
    ]
    twin_exclude_joint_list = []
    for baby_name in baby_names:
        for exclude_joint in exclude_joint_list:
            twin_exclude_joint_list.append(f"{baby_name}_{exclude_joint}")

    # Make natural posture
    for baby_name in baby_names:
        env.data.joint(f"{baby_name}_pelvis_tilt_AXIS_rotation1").qpos[0] = 0
        env.data.joint(f"{baby_name}_pelvis_list_AXIS_rotation2").qpos[0] = 0
        env.data.joint(f"{baby_name}_pelvis_rotation_AXIS_rotation3").qpos[0] = 0
        env.data.joint(f"{baby_name}_pelvis_tx_AXIS_translation1").qpos[0] = 0
        env.data.joint(f"{baby_name}_pelvis_ty_AXIS_translation2").qpos[0] = 0
        env.data.joint(f"{baby_name}_pelvis_tz_AXIS_translation3").qpos[0] = 0
        env.data.joint(f"{baby_name}_shoulder_elv_R_AXIS_rotation1").qpos[0] = 0.283
        env.data.joint(f"{baby_name}_shoulder_elv_L_AXIS_rotation1").qpos[0] = 0.283
        env.data.joint(f"{baby_name}_shoulder1_R2_AXIS_rotation2").qpos[0] = -1.57
        env.data.joint(f"{baby_name}_shoulder1_L2_AXIS_rotation2").qpos[0] = -1.57

    # Build the video writer
    n_skipped = 0
    if args.headless:
        frame_rate = 25
        n_skip_images = int((1 / env.dt) // frame_rate)
        video_writer = cv2.VideoWriter(
            f"{data_path}/tactile_{args.camera_position}_{cmap_name}.mp4",
            cv2.VideoWriter_fourcc(*"mp4v"),
            frame_rate,
            (args.render_width, args.render_height),
        )
    else:
        n_skip_images = 1  # No skip

    # Define the state of physics simulator for each step
    def forward_simulation():
        if args.replay_tactile:
            for valid_tactile_id, valid_only_display_site_id in zip(
                valid_tactile_ids, valid_only_display_site_ids
            ):
                tactile_item = tactile_clip[step][valid_tactile_id]
                env.model.site_rgba[valid_only_display_site_id] = get_site_rgba(
                    tactile_item
                )
        env.forward()

    # Start the loop
    end_step = len(tactile)
    for step in range(end_step):
        if args.headless:
            if n_skipped % n_skip_images == 0:
                forward_simulation()
                data_value = env.render(
                    mode="rgb_array",
                    camera_id=camera_id,
                    render_width=args.render_width,
                    render_height=args.render_height,
                )
                img = Image.fromarray(data_value)
                idx = step
                img.save(DATA + f'/touch{idx:05}.png')
                data_value = cv2.cvtColor(data_value, cv2.COLOR_RGB2BGR)
                #video_writer.write(data_value)
                n_skipped = 0
        else:
            forward_simulation()
            env.render()

        n_skipped += 1
        env.data.time += 0.04
        step += 1

        if step % 1000 == 0:
            print(f"{step}/{end_step}")

    if args.headless:
        video_writer.release()


if __name__ == "__main__":
    bool_type = lambda x: (str(x).lower() in ["true", "1", "yes"])
    parser = argparse.ArgumentParser()

    # Log
    parser.add_argument("--log_name", default="default", type=str)
    parser.add_argument("--log_base_dir", default=".", type=str)

    # Render setting
    parser.add_argument("--headless", default=True, type=bool_type)
    parser.add_argument(
        "--camera_position",
        default="front",
        type=str,
        choices=["front", "back", "side"],
    )
    parser.add_argument("--replay_joint_angle", default=False, type=bool_type)
    parser.add_argument("--replay_tactile", default=True, type=bool_type)
    parser.add_argument("--render_width", default=1920, type=int)
    parser.add_argument("--render_height", default=1080, type=int)

    # Environment
    parser.add_argument("--model_path", default="isi_infant.xml", type=str)

    # Initial joint value
    parser.add_argument("--initial_joint_values", metavar="KEY=VALUE", nargs="*")

    # Build the arguments
    args = parser.parse_args()
    parse_initial_joint_values(args)

    # Custom arguments
    args.model_path = "230126_04_isi_infant_floor_mocap.xml"
    args.log_name = "default"
    # args.log_base_dir = "."
    # args.headless = False
    # args.camera_position = "side"
    # args.replay_joint_angle = True

    execute(args)
