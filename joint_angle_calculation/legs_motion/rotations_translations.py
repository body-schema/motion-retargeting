import numpy as np

def Rx(phi):
    R = np.array([
        [1, 0,            0,           0],
        [0, np.cos(phi), -np.sin(phi), 0],
        [0, np.sin(phi),  np.cos(phi), 0],
        [0, 0,            0,           1]
    ], dtype=np.float64)
    return R

def Ry(phi):
    R = np.array([
            [ np.cos(phi), 0, np.sin(phi), 0],
            [ 0,           1, 0,           0],
            [-np.sin(phi), 0, np.cos(phi), 0],
            [ 0,           0, 0,           1]
        ], dtype=np.float64)
    return R

def Rz(phi):
    R = np.array([
            [np.cos(phi), -np.sin(phi), 0, 0],
            [np.sin(phi),  np.cos(phi), 0, 0],
            [0,            0,           1, 0],
            [0,            0,           0, 1]
        ], dtype=np.float64)
    return R

def Tx(a):
    T = np.eye(4,4, dtype=np.float64)
    T[0,3] = a
    return T

def Tz(a):
    T = np.eye(4,4, dtype=np.float64)
    T[2,3] = a
    return T