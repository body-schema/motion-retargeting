import matplotlib.pyplot as plt
import numpy as np
import sys


FROM = 1000
TO = 5000

FRAMERATE = 1
FONTSIZE = 30
FONTSIZE2 = 20
LINEWIDTH = 4
CSV = True
def load_arm_joint_angles(file):
    left_arm = [[] for i in range(4)]
    right_arm = [[] for i in range(4)]
    left = True
    first = True

    for line in file:
        my_line = line.split(',')
        if first and CSV:
            first = False
            continue
        if left:
            for j in range(4):
                left_arm[j].append(float(my_line[j]))
        else:
            for j in range(4):
                right_arm[j].append(float(my_line[j]))
        left = not left

    return left_arm, right_arm

def load_yaws_from_matlab(file):
    left_arm_yaw = []
    right_arm_yaw = []
    left = True
    for line in file:
        my_line = line.split(',')
        print(len(my_line))
        if left:    
            left_arm_yaw = [float(i) for i in my_line]
        else:
            right_arm_yaw = [float(i) for i in my_line]
        left = not left
    return left_arm_yaw, right_arm_yaw


def plot(arm, ylab, name, yaws = None, horizontals = None):
    time = [i/FRAMERATE for i in range(TO-FROM)]
    fig, _ = plt.subplots(4, 1, figsize=(3840/1.5/96, 2160/96))
    fig.suptitle(ylab + 'arm joint angles', fontsize=50)
    
    min_border = [-28, -84, -99, -146]
    max_border = [118, 183, 67, 5]
    ylabels = ['sh. pitch', 'sh. roll', 'sh. yaw', 'el. angle']


    for i in range(4):
        plt.subplot(4, 1, i+1)
        if i == 0 or i == 3:
            data = [-thing for thing in arm[i][FROM:TO]]
        elif i == 1:
            data = arm[i][FROM:TO]
        elif i == 2:
            data = [thing - 90 for thing in arm[i][FROM:TO]]
        plt.plot(time, data, label = 'calculated', linewidth=LINEWIDTH)
        plt.plot(time, [min_border[i] for j in range(TO-FROM)], 'r--', label='iCub limits')
        plt.plot(time, [max_border[i] for j in range(TO-FROM)], 'r--')
        if i == 1:
            plt.plot(time, [100 for j in range(TO-FROM)], 'g--', label='gimbal lock')
            plt.plot(time, [80 for j in range(TO-FROM)], 'g--')

        
        if i == 2 and yaws is not None:
            print(yaws)
            plt.plot(time, yaws[FROM:TO], label = 'matlab', linewidth = LINEWIDTH, linestyle = 'dashed')
        if i == 0 and horizontals is not None:
            plt.plot(time, horizontals[FROM:TO], label = 'matlab', linewidth = LINEWIDTH, linestyle = 'dashed')
        plt.xticks(fontsize=FONTSIZE2)
        plt.yticks(fontsize=FONTSIZE2)

        plt.xlim(0, time[-1])
        plt.ylim(min(min(data), min_border[i])-10, max(max(data), max_border[i])+10)

        plt.ylabel(ylabels[i] + ' (°)', fontsize=FONTSIZE)
        plt.grid()
        plt.legend(loc='upper right', fancybox=True, framealpha=0.5, fontsize=FONTSIZE2)
    plt.xlabel('time (s)', fontsize=FONTSIZE)
    plt.show()
    #fig.savefig(name + '.png')
    #fig.savefig(name + '.pdf', format='pdf', transparent=True)
    #fig.savefig(name + '.eps', format='eps', transparent=True)

def main():
    address = sys.argv[1]
    adress2 = sys.argv[2]
    adress3 = sys.argv[3]
    f2 = open(adress2, 'r')
    f3 = open(adress3, 'r')
    with open(address, 'r') as f:
        left_arm, right_arm = load_arm_joint_angles(f)
        l_yaw, r_yaw = load_yaws_from_matlab(f2)
        l_horizontal, r_horizontal = load_yaws_from_matlab(f3)
        plot(left_arm, "Left", 'graphs/lim_unlim/04_left_lim')#l_yaw, l_horizontal)
        plot(right_arm, "Right", 'graphs/lim_unlim/04_right_lim')# r_yaw, r_horizontal)
    return None

if __name__ == '__main__':
    main()