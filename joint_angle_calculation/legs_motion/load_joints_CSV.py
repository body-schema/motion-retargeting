from ast import JoinedStr
import numpy as np
import csv
import sys
from os import mkdir, path

import leg_joint_angles as lja

CROP = False

global joints
joints = ['nose_vertex',     # nose
          'neck',
          'right_shoulder',
          'right_elbow',
          'right_wrist',
          'left_shoulder',
          'left_elbow',
          'left_wrist',
          'mid_hip',          
          'right_hip',    
          'right_knee',   
          'right_ankle',  
          'left_hip',     
          'left_knee',      
          'left_ankle',     
          'right_eye',
          'left_eye',
          'right_ear',
          'left_ear',
          'left_bigtoe',
          'left_smalltoe',
          'left_heel',
          'right_bigtoe',
          'right_smalltoe',
          'right_heel']

def load_joints(address):
    if not path.exists('data'):
        mkdir('data')
    if len(sys.argv) > 2 and sys.argv[2] == '1':
        f = open('data/leg_angles.csv', 'a', newline='')
        first = False
    else:
        f = open('data/leg_angles.csv', 'w', newline='')
        first = True
    loaded_joints = {}

    writer1=csv.writer(f)
    head = ['Hip pitch', 'Hip roll', 'Hip yaw', 'Knee angle'] 

    if first:
        writer1.writerow(head)

    with open(address, 'r') as file:
        csvreader = csv.reader(file)
        header = next(csvreader)
        global joints
        for row in csvreader:
            row = row[0].split('\t')
            for i in range(len(joints)):
                loaded_joints[joints[i]] = (np.array(row[3*i : 3*i+3])).astype(float)
            leftLeg = lja.Leg(loaded_joints, True, crop_icub=CROP)
            writer1.writerow(leftLeg.angles[0:len(head)])

            rightLeg = lja.Leg(loaded_joints, False, crop_icub=CROP)
            row = rightLeg.angles
            writer1.writerow(row[0:len(head)])
    f.close()

if __name__ == '__main__':
    load_joints(sys.argv[1])