import numpy as np
import math_utils as mu
import rotations_translations as rt

class Arm():
    '''
    when calling it counts the angles of elbow and shoulder pitch, roll and yaw
    it sets values of wrist angles to zeros and the other angles to create a fist
    '''
    def __init__(self, joints, left, crop_icub=False, horizontal=False) -> None:
        self.joints = joints
        self.left = left
        self.crop_icub = crop_icub
        self.horizontal = horizontal
        self.vec_up = None          # * from elbow to shoulder
        self.vec_fore = None        # * from elbow to hand
        self.vec_spine = None       # * from spine to head
        self.vec_shoulder = None    # * from left to right or right to left
        self.vec_finger = None      # * from hand to finger
        self.ref_roll = None        # * reference vector for roll
        self.shoulder = None
        self.init_vectors()
        self.angles = [0 for i in range(16)]
        self.count_angles()
        self.angles[7:16] = [60., 90., 90., 180., 90., 180., 90., 180., 270.]   # the angles to hold a fist

    def init_vectors(self):
        '''
        function to initialize all the vectors
        :return: None
        '''
        self.vec_shoulder = np.subtract(self.joints['left_shoulder'], self.joints['right_shoulder'])

        if self.left:
            self.shoulder = self.joints['left_shoulder']
            elbow = self.joints['left_elbow']
            hand = self.joints['left_wrist']
            # fingers = self.joints['left_fingers']

        else:
            self.shoulder = self.joints['right_shoulder']
            elbow = self.joints['right_elbow']
            hand = self.joints['right_wrist']
            # fingers = self.joints['right_fingers']
            self.vec_shoulder *= -1     # different direction in case of right arm
        
        self.vec_up = np.subtract(self.shoulder, elbow)
        self.vec_fore = np.subtract(elbow, hand)
        self.vec_spine = np.subtract(self.joints['neck'], self.joints['mid_hip'])
        # self.vec_finger = np.subtract(fingers, hand)
        return None

    def count_elbow_angle(self):
        '''
        :return: elbow angle in degrees
        '''
        self.angles[3] = np.rad2deg(mu.count_angle_unsigned(self.vec_up, self.vec_fore))     # max(min(self.angles[3], 106), 15)
        if self.crop_icub:
            self.angles[3] = mu.crop_to_interval(self.angles[3], 106, 15)
        return self.angles[3]

    def shoulder_pitch(self):
        '''
        :return: angle of shoulder pitch in degrees
        '''
        vec = mu.project_vect_to_plane(-self.vec_up, self.vec_shoulder)
        self.ref_roll = vec
        reference_vector = mu.project_vect_to_plane(-self.vec_spine, self.vec_shoulder)
        if self.left:
            self.angles[0] = np.rad2deg(mu.count_angle_signed(reference_vector, vec, self.vec_shoulder))
        else:
            self.angles[0] = np.rad2deg(mu.count_angle_signed(reference_vector, vec, -self.vec_shoulder))
        # -shoulder as normal because of R-oriented rotation
        # max(min(8, self.angles[0]), -95.5)
        if self.crop_icub:
            self.angles[0] = mu.crop_to_interval(self.angles[0], 8, -95.5)
        return self.angles[0]

    def shoulder_roll(self):
        '''
        :return: angle of shoulder roll in degrees
        '''
        #self.angles[1] = (np.rad2deg(mu.count_angle_unsigned(vec, ref, normal)))
        self.angles[1] = np.rad2deg(mu.count_angle_unsigned(self.ref_roll, -self.vec_up))
        
        if self.crop_icub:
            self.angles[1] = mu.crop_to_interval(self.angles[1], 160, 0)
        return self.angles[1]

    def shoulder_yaw(self):
        '''
        :return: angle of shoulder yaw in degrees
        '''
        # move the coordinate frame to the shoulder position
        shoulder_turn = np.eye(4,4)
        if self.left:
            vec = -self.vec_shoulder
        else:
            vec = self.vec_shoulder
        shoulder_turn[0:3,3] = self.shoulder
        R = mu.axis_angle_rotation_mat(np.array([0,1,0]), vec)
        if self.left:
            base = shoulder_turn @ R @ rt.Rz(-np.pi/2) @ rt.Rx(np.pi/2) @ rt.Ry(-np.pi/2)
        else:
            base = shoulder_turn @ R @ rt.Rz(np.pi/2) @ rt.Rx(np.pi/2) @ rt.Ry(np.pi/2)
        # apply pitch
        base = base @ rt.Rz(np.deg2rad(-self.angles[0]))
        if self.left:
            base = base @ rt.Rx(-np.pi/2)
        else:
            base = base @ rt.Rx(np.pi/2)
        #  apply roll
        base = base @ rt.Rz(np.deg2rad(self.angles[1])) @ rt.Tx(np.linalg.norm(self.vec_up))
        #  apply elbow
        mid_base = base @ rt.Rz(np.deg2rad(self.angles[3])) @ rt.Tx(np.linalg.norm(self.vec_fore))
        without_yaw = mid_base[0:3, 3] - base[0:3,3]
        # count yaw
        normal = base[0:3, 0]
        projection1 = mu.project_vect_to_plane(without_yaw, normal)
        projection2 = mu.project_vect_to_plane(-self.vec_fore,normal)
        angle = mu.count_angle_signed(projection1,projection2, -normal)
        if self.left:
            angle= -angle
        self.angles[2] = np.rad2deg(angle)

        return angle

    def shoulder_add_ab(self):
        '''
        :return: angle of shoulder adduction/abduction in degrees
        '''
        ref = -self.vec_spine
        vec = -self.vec_up
        
        self.angles[1] = np.rad2deg(mu.count_angle_unsigned(ref,vec))
        
        return self.angles[1]
    
    def shoulder_horizontal_and_yaw(self):
        '''
        :return: angles of shoulder horizontal and yaw in degrees
        '''
        # init vector to construct rotation
        if self.left:
            sh_vec = -self.vec_shoulder
        else:
            sh_vec = self.vec_shoulder
        
        R = mu.axis_angle_rotation_mat(np.array([0., 1., 0.]), sh_vec)

        shoulder_turn = np.eye(4,4)
        shoulder_turn[0:3, 3] = self.shoulder

        # move the coordinate frame to the shoulder joint
        if self.left:
            base = shoulder_turn @ R @ rt.Rz(-np.pi/2) @ rt.Rx(np.pi/2) @ np.eye(4,4)
        else:
            base = shoulder_turn @ R @ rt.Rz(np.pi/2) @ rt.Rx(np.pi/2) @ np.eye(4,4)

        # then project to the spine shoulder vector and upper arm vector
        normal = base[0:3, 2]
        normal /=  np.linalg.norm(normal)
        upp_arm = -self.vec_up
        project1 = mu.project_vect_to_plane(upp_arm, normal)
        if self.left:
            sh_vec *= -1
        project2 = mu.project_vect_to_plane(sh_vec, normal)

        if not self.left:
            theta = mu.count_angle_signed(project1,project2, -normal)
        else:
            theta = mu.count_angle_signed(project1,project2, -normal)
        theta = np.rad2deg(theta)
        self.angles[0] = theta

        # go to elbow joint
        base = base @ rt.Rz(np.deg2rad(theta)) @ rt.Rx(np.pi/2)
        base = base @ rt.Rz(np.pi/2 - mu.count_angle_unsigned(normal, -self.vec_up))\
              @ rt.Rx(-np.pi/2) @ rt.Tx(np.linalg.norm(self.vec_up))

        # apply elbow angle
        low_arm = -self.vec_fore
        elbow_angle = self.angles[3]
        mid_base = base @ rt.Rz(np.deg2rad(elbow_angle)) @ rt.Tx(np.linalg.norm(low_arm))

        # find rotation / yaw
        without_yaw = mid_base[0:3, 3] - base[0:3, 3]
            # projection to upp_arm vector 
        v = base[0:3, 0]
        normal_v = v / np.linalg.norm(v)

        projected1 = mu.project_vect_to_plane(without_yaw, normal_v)
        projected2 = mu.project_vect_to_plane(low_arm, normal_v)
        if self.left:
            yaw = mu.count_angle_signed(projected1, projected2, normal_v)
        else:
            yaw = mu.count_angle_signed(projected1, projected2, -normal_v)
        self.angles[2] = np.rad2deg(yaw)

        return self.angles[0], self.angles[2] 
    
    def count_angles(self):
        self.count_elbow_angle()
        if self.horizontal:
            self.shoulder_add_ab()
            self.shoulder_horizontal_and_yaw()
        else:
            self.shoulder_pitch()
            self.shoulder_roll()
            self.shoulder_yaw()

    def check_correct_limits_for_icub(self):
        c = 1.71
        A = np.matrix([[c, -c, 0], [-1, 1, 0], [-c, c, 0], [1, -1, 0], [0, -1, 0], [0, 1, 0], [0, 1, 0], [0, -1, 0], [c, -c, -c], [-1, 1, 1], [-c, c, c], [1, -1, -1], [0, 1, 1], [0, -1, -1]])
        b = np.matrix([[404], [54.3], [46], [305.7], [215.7], [150], [54.3], [210], [431], [101.7], [109], [258.3], [71.7], [228.3]])
        q = np.matrix([[self.angles[0]], [self.angles[1]], [self.angles[2]]])

        X = np.matmul(A, q) + b
        for point in X:
            if point <= 0:
                self.angles[3] = 0
                return False
        return False