import os
import numpy as np
import mujoco_py

from mimoEnv.envs.mimo_env import MIMoEnv, DEFAULT_PROPRIOCEPTION_PARAMS, SCENE_DIRECTORY, DEFAULT_VISION_PARAMS, DEFAULT_VESTIBULAR_PARAMS
import mimoEnv.utils as env_utils

""" List of possible target bodies.

:meta hide-value:
"""

RESET_POSITION = {"mimo_location": np.array([0.0579584, -0.00157173, 0.0566738, 0.892294, -0.0284863, -0.450353, -0.0135029])}

SELFBODY_XML = os.path.join(SCENE_DIRECTORY, "retargeting.xml")
""" Path to the scene for this experiment.

:meta hide-value:
"""

class MIMoRetargeting(MIMoEnv):
    """ MIMo learns about his own body.

    MIMo is tasked with touching a given part of his body using his right arm.
    Attributes and parameters are mostly identical to the base class, but there are two changes.
    The constructor takes two arguments less, ``goals_in_observation`` and ``done_active``, which are both permanently
    set to `True`.
    Finally there are two extra attributes for handling the goal state. The :attr:`.goal` attribute stores the target
    geom in a one hot encoding, while :attr:`.target_geom` and :attr:`.target_body` store the geom and its associated
    body as an index. For more information on geoms and bodies please see the MuJoCo documentation.

    Attributes:
        target_geom (int): The body part MIMo should try to touch, as a MuJoCo geom.
        target_body (str): The name of the kinematic body that the target geom is a part of.
        init_sitting_qpos (numpy.ndarray): The initial position.
    """

    def __init__(self,
                 model_path=SELFBODY_XML,
                 initial_qpos={},
                 n_substeps=1,
                 proprio_params=DEFAULT_PROPRIOCEPTION_PARAMS,
                 touch_params=DIFFERNT_TOUCH,
                 vision_params=DEFAULT_VISION_PARAMS,
                 vestibular_params=DEFAULT_VESTIBULAR_PARAMS,
                 ):

        super().__init__(model_path=model_path,
                         initial_qpos=initial_qpos,
                         n_substeps=n_substeps,
                         proprio_params=proprio_params,
                         touch_params=touch_params,
                         vision_params=vision_params,
                         vestibular_params=vestibular_params,
                         goals_in_observation=False,
                         done_active=False)
        print('differenet touch bracho')
        self.init_pose = self.sim.data.qpos.copy()

    def _step_callback(self):
        """ Manually reset position excluding arm each step.

        This restores the body to the sitting position if it deviated.
        Avoids some physics issues that would sometimes occur with welds.
        """
        # Manually set body to sitting position (except for the right arm joints)
        for body_name in RESET_POSITION:
            env_utils.set_joint_qpos(self.sim.model, self.sim.data, body_name, SITTING_POSITION[body_name])

    def _reset_sim(self):
        """ Reset to the initial sitting position.

        Returns:
            bool: `True`
        """
        # set qpos as new initial position and velocity as zero
        qpos = self.init_sitting_qpos
        qvel = np.zeros(self.sim.data.qvel.shape)

        new_state = mujoco_py.MjSimState(
            self.initial_state.time, qpos, qvel, self.initial_state.act, self.initial_state.udd_state
        )

        self.sim.set_state(new_state)
        self.sim.forward()

        return True
