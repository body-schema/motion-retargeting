from ast import JoinedStr
import numpy as np
import csv
import sys
from os import mkdir, path

import neck_n_torso_joint_angles as ntja

CROP = False

global joints
joints = ['nose_vertex',     # nose
          'neck',
          'right_shoulder',
          'right_elbow',
          'right_wrist',
          'left_shoulder',
          'left_elbow',
          'left_wrist',
          'mid_hip',          
          'right_hip',    
          'right_knee',   
          'right_ankle',  
          'left_hip',     
          'left_knee',      
          'left_ankle',     
          'right_eye',
          'left_eye',
          'right_ear',
          'left_ear',
          'left_bigtoe',
          'left_smalltoe',
          'left_heel',
          'right_bigtoe',
          'right_smalltoe',
          'right_heel']

def load_joints(address):
    if not path.exists('data'):
        mkdir('data')
    if len(sys.argv) > 2 and sys.argv[2] == '1':
        f = open('data/body_joint_angles.csv', 'a', newline='')
        first = False
    else:
        f = open('data/body_joint_angles.csv', 'w', newline='')
        first = True
    loaded_joints = {}

    writer1=csv.writer(f)
    head = ['Torso pitch', 'Torso roll', 'Torso yaw',
            'Neck pitch', 'Neck roll', 'Neck yaw']

    if first:
        writer1.writerow(head)

    with open(address, 'r') as file:
        csvreader = csv.reader(file)
        header = next(csvreader)
        global joints
        for row in csvreader:
            row = row[0].split('\t')
            for i in range(len(joints)):
                loaded_joints[joints[i]] = (np.array(row[3*i:3*i+3])).astype(float)
            body = ntja.Torso_Neck(loaded_joints, crop=CROP)
            writer1.writerow(body.angles[0:len(head)])
    f.close()

if __name__ == '__main__':
    load_joints(sys.argv[1])