import gym
import time
import numpy as np
import mimoEnv
import utils # MIMo utils
import mujoco_py as mjp
from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib import cm
import matplotlib.tri as mtri
from mpl_toolkits.mplot3d import Axes3D
import csv
import pickle
COLORS = ['r', 'g', 'b', 'c', 'm' \
        'purple' ]


DATA = "data"


def plot_touch_body(touch_params = None, env=None, idx=0):
    """plots or saves touch figures with red spots indicating the place of contact

    Args:
        touch_params (dict, optional): The dictionary saved in motion_retargeting.py. Defaults to None.
        env (gym environment, optional): Environment, MIMo. Defaults to None.
        idx (int, optional): index. Defaults to 0.
    """
    if env is None:
        env = gym.make('MIMoMotionRetargeting-v1')
        env.seed(42)
        _ = env.reset()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    all_points = {}
    if touch_params is None:
        touch_params = []
    
    find_touch_location(touch_params, env, all_points)

    global_points = find_points_whole_body(env, all_points)

    all_points['silver'] = global_points
    plot_points(all_points,ax)

    ax.set_aspect('equal', adjustable='box')
    ax.view_init(elev=63, azim=-1, roll=0)
    plt.axis('off')
    plt.savefig(f'{DATA}/data_touch/{idx:03}.png',bbox_inches='tight',dpi=2500)

def find_doubles(touch_params):
    '''
    only return touch parameters which are complementary -- we know, what is touching what (excluding random parts touching world frame)
    '''
    new_touch = []
    for i in range(len(touch_params)):
        if i == 0:
            if len(touch_params) > 1 and touch_params[i][0] == touch_params[i+1][0]:
                new_touch.append(touch_params[i])
        elif i == len(touch_params) - 1:
            if len(touch_params) > 1 and touch_params[i][0] == touch_params[i-1][0]:
                new_touch.append(touch_params[i])
        else:
            if touch_params[i][0] == touch_params[i-1][0] or touch_params[i][0] == touch_params[i+1][0]:
                new_touch.append(touch_params[i])
    
    return new_touch

def find_touch_location(touch_paramsx, env, all_points):
    '''
    finds the touch locations
    '''
    for i in range(len(touch_paramsx)//2+1):
        all_points['r'] = []
    touch_params = find_doubles(touch_paramsx)
    for j in range(len(touch_params)):
        touch_param = touch_params[j]
        idx = touch_param[0]
        body_id = touch_param[1]
        pos_in_body = touch_param[3]
        k = np.min([len(env.touch.sensor_positions[body_id])-1,5])
        try:
            sens, _ = env.touch.get_k_nearest_sensors(pos_in_body,body_id,k)
            for indx in sens:
                sensor_pos = utils.body_pos_to_world(env.sim.data, \
                                                    env.touch.sensor_positions[body_id][indx], \
                                                    body_id)
                all_points['r'].append(sensor_pos)
        except:
            continue
    for i in range(len(touch_params)//2+1):
        all_points['r'] = np.array(all_points['r'])

def plot_points(all_points, ax):
    counter = 1
    for key in all_points.keys():
        if len(all_points[key]) < 1:
            continue
        xs = all_points[key][:,0]
        ys = all_points[key][:,1]
        zs = all_points[key][:,2]
        if key == 'silver':
            ax.scatter(xs, ys, zs, color=key, s=20, label='No contact detected')
        else:
            ax.scatter(xs, ys, zs, color=key, s=20, label=f'Contact detected')
            counter += 1

def find_points_whole_body(env, all_points):
    """
    finds the points that are not touched
    """
    global_points = []
    counter = 0
    for i in range(4,25):
        name = env.sim.model.body_id2name(i)
        points = env.touch.sensor_positions[i]
        glob_points = np.zeros(points.shape)
        for j in range(points.shape[0]):
            global_point = utils.body_rot_to_world(env.sim.data, points[j, :], i) + utils.get_body_position(env.sim.data, i)
            if not is_there(global_point, all_points):
                glob_points[j, :] = (global_point)
            else:
                counter +=1
                glob_points[j,:] = [np.nan, np.nan, np.nan]
        global_points.append(glob_points)
    global_points = np.array(global_points)
    global_points = np.concatenate(global_points)
    return global_points

def is_there(point, where):
    """determine if there is a point in the dictionary
    """
    for key in where.keys():
        for base in where[key]:
            if np.linalg.norm(base - point) < 0.001:
                return True
    return False

def create_surface(all_points, ax):
    """plots the surface with points
    """
    x,y,z = [],[],[]
    ## all points create
    for key in all_points.keys():
        x.append(all_points[key][:, 0])
        y.append(all_points[key][:, 1])
        z.append(all_points[key][:, 2])
    x = np.concatenate(x)
    mask = np.isnan(x)
    x = x[~mask]
    y = np.concatenate(y)
    mask = np.isnan(y)
    y = y[~mask]
    z = np.concatenate(z)
    mask = np.isnan(z)
    z = z[~mask]
    ## triangulation
    triangulation = mtri.Triangulation(x,y)
    ## now values
    counter = 1
    for key in all_points:
        c_x, c_y, c_z = all_points[key][:,0], all_points[key][:,1], all_points[key][:,2]
        if counter == 1:
            values = np.where(np.isin(x, c_x) & np.isin(y, c_y) & np.isin(z, c_z), counter, 0)
        else:
            values = np.where(np.isin(x, c_x) & np.isin(y, c_y) & np.isin(z, c_z), counter, values)
        if key == 'silver':
            values = np.where(np.isin(x, c_x) & np.isin(y, c_y) & np.isin(z, c_z), 0, values)
        counter+=1
    
    ax.tricontourf(triangulation, values)
    return None

if __name__ == "__main__":
    env = gym.make('MIMoMotionRetargeting-v0')
    env.seed(42)
    _ = env.reset()
    with open('data_20w/touch_data.pkl','rb') as fp:
        touch = pickle.load(fp)
        print(touch.keys())
        for i in range(touch['frames']):
            if i in touch.keys() and len(touch[i]) > 0:
                #print(i)
                plot_touch_body(touch[i], env,i)
            else:
                plot_touch_body(None, env,i)
