cmake_minimum_required(VERSION 3.5)

project(whole_body_motion)

find_package(YARP 3.1.101 REQUIRED)
find_package(ICUBcontrib REQUIRED)

list(APPEND CMAKE_MODULE_PATH ${ICUBCONTRIB_MODULE_PATH})

include(ICUBcontribHelpers)
include(ICUBcontribOptions)

icubcontrib_set_default_prefix()

add_executable(whole_body_motion whole_body_motion.cpp)
target_compile_definitions(whole_body_motion PRIVATE _USE_MATH_DEFINES)
target_link_libraries(whole_body_motion ${YARP_LIBRARIES})
install(TARGETS whole_body_motion DESTINATION bin)

icubcontrib_add_uninstall_target()
file(GLOB scripts ${CMAKE_SOURCE_DIR}/app/scripts/*.xml)
yarp_install(FILES ${scripts} DESTINATION ${ICUBCONTRIB_APPLICATIONS_INSTALL_DIR})
