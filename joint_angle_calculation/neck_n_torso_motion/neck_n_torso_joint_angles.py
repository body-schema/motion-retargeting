import numpy as np
import math_utils as mu

class Torso_Neck():
    def __init__(self, joints, crop=True, csv=True) -> None:
        self.joints = joints
        self.crop = crop
        self.csv = csv

        # vectors
        self.vec_spine = None       # middle hip to neck
        self.vec_shoulder = None    # left to right shoulder
        self.vec_hip = None         # left to right hip
        self.vec_lowspine = None    # mid hip to middle of the left and right hips
        if csv:
            self.vec_eyes = None
            self.vec_ears = None
            self.vec_nose = None
        self.init_vectors()

        # angles
        self.angles = [0 for i in range(9)]
        self.init_angles()

    def init_vectors(self):
        '''
        function to initialize all the vectors needed
        :return: None
        '''
        self.vec_spine = np.subtract(self.joints['neck'], self.joints['mid_hip'])
        self.vec_shoulder = np.subtract(self.joints['right_shoulder'], self.joints['left_shoulder'])
        self.vec_hip = np.subtract(self.joints['right_hip'], self.joints['left_hip'])
        middle_hip = np.add(self.joints['left_hip'], self.joints['right_hip']) / 2
        self.vec_lowspine = np.subtract(middle_hip, self.joints['mid_hip'])
        if self.csv:
            self.vec_ears = np.subtract(self.joints['right_ear'], self.joints['left_ear'])
            self.middle_ear = np.add(self.joints['left_ear'], self.vec_ears/2)
            self.vec_nose = np.subtract(self.joints['nose_vertex'], self.middle_ear)
            self.vec_eyes = np.subtract(self.joints['right_eye'], self.joints['left_eye'])
        return None

    def torso_pitch(self):
        # reference
        ref = mu.project_vect_to_plane(-self.vec_lowspine, -self.vec_hip)

        # vector
        vec = mu.project_vect_to_plane(self.vec_spine, -self.vec_hip)

        self.angles[0] = np.rad2deg(mu.count_angle_signed(ref, vec, -self.vec_hip))
        return self.angles[0]

    def torso_yaw(self):
        # reference
        ref = mu.project_vect_to_plane(self.vec_hip, self.vec_spine)

        # vector
        vec = mu.project_vect_to_plane(self.vec_shoulder, self.vec_spine)

        self.angles[2] = np.rad2deg(mu.count_angle_signed(ref, vec, self.vec_spine))
        return self.angles[2]

    def torso_roll(self):
        normal = np.cross(self.vec_hip, self.vec_lowspine)
        ref = -self.vec_lowspine
        # vector
        vec = mu.project_vect_to_plane(self.vec_spine, normal)

        self.angles[1] = np.rad2deg(mu.count_angle_signed(ref, vec, -normal))
        
        return self.angles[1]

    # neck joints active only while having CSV - MINIRGBD-dataset doesn't contain eyes and ear
    def neck_pitch(self):
        ref = self.vec_spine

        # make a plane
        vec_eyeL = np.subtract(self.joints['left_eye'], self.joints['neck'])
        vec_eyeR = np.subtract(self.joints['right_eye'], self.joints['neck'])
        vec = np.cross(vec_eyeL, vec_eyeR)

        # measure pitch
        angle = 180 - np.rad2deg(mu.count_angle_unsigned(vec, ref))
        angle = angle - 81
        self.angles[3] = angle
        return angle

    def neck_roll(self):
        normal = np.cross(self.vec_shoulder, self.vec_spine)

        ref = self.vec_spine

        to_project = np.subtract(self.middle_ear, self.joints['neck'])

        vec = mu.project_vect_to_plane(to_project, normal)

        self.angles[4]= np.rad2deg(mu.count_angle_signed(ref, vec, normal))

        return self.angles[4]

    def neck_yaw(self):
        ref = mu.project_vect_to_plane(self.vec_shoulder, self.vec_spine)

        vec = mu.project_vect_to_plane(self.vec_eyes, self.vec_spine)

        self.angles[5] = np.rad2deg(mu.count_angle_signed(ref, vec, self.vec_spine))

        return self.angles[5]

    def init_angles(self):
        self.torso_yaw()
        self.torso_roll()
        self.torso_pitch()
        self.neck_pitch()
        self.neck_roll()
        self.neck_yaw()
