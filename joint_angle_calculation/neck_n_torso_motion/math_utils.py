import numpy as np

def count_angle_unsigned(vec1, vec2):
    '''
    function to count angle between two vectors using cosine function
    :param vec1: first vector
    :param vec2: second vector
    :return: angle in radians between vectors
    '''
    v1 = vec1/np.linalg.norm(vec1)
    v2 = vec2/np.linalg.norm(vec2)
    num = np.dot(v1, v2)
    angle = np.arccos(num)
    return angle

def count_angle_signed(vec1, vec2, v_norm):
    '''
    function to count signed angle between two vectors in a plane given by normal
    :param vec1: vector we want to rotate
    :param vec2: vector we want to rotate to
    :param v_norm: normal of a plane we rotate in
    :return: signed angle in radians in R-oriented plane
    '''
    v1 = vec1/np.linalg.norm(vec1)
    v2 = vec2/np.linalg.norm(vec2)
    vn = v_norm/np.linalg.norm(v_norm)

    angle = np.arctan2(np.dot(np.cross(v1, v2), vn), np.dot(v1, v2))

    return angle

def project_vect_to_plane(vec, normal):
    '''
    function to project vector to plane given by normal
    :param vec: vector ve want to project
    :param normal: normal of a given plane
    :return: projected vector as np.array
    '''

    projec_to_normal = np.dot(vec, normal)/np.linalg.norm(normal)**2 * normal
    projected_vec = np.subtract(vec, projec_to_normal)
    return projected_vec

def crop_to_interval(val, max_val, min_val):
    angle = val
    minimum = min_val
    maximum = max_val

    if min_val <= val <= max_val:
        # it's in the interval
        return angle
    
    if minimum < 0:
        minimum += 360
    if maximum < 0:
        maximum += 360
    if angle < 0:
        angle += 360

    alpha = min(abs(minimum - angle), 360 - abs(minimum - angle))
    beta = min(abs(maximum - angle), 360 - abs(maximum - angle))

    if alpha < beta:
        return min_val
    else:
        return max_val

def norm_angle(angle):
    an = angle
    while an < -180:
        an += 360
    while an > 180:
        an -= 360

    return an