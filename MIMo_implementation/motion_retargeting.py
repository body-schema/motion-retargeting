"""
Script to replay the retargetted movement
"""

import gym
import time
import numpy as np
import mimoEnv
import utils
import mujoco_py as mjp
from plot_body import plot_touch_body
import pickle
from PIL import Image 
import PIL 

import csv

PATH_TO_XML = "/home/ondra/Plocha/MIMo/mimoEnv/assets/retargeting.xml" # path to the XML definition
DATA = "data_20w" # path to data directory with angles
SAVE_TOUCH = False # save the touch params
SAVE_VISION = False # save the images from eyes
SAVE_MIMO = False # save the body pose

def viewer_setup(viewer):
    # setup the camera viewer
    viewer.cam.trackbodyid = -1         # id of the body to track ()
    viewer.cam.distance = 0      # how much you "zoom in", model.stat.extent is the max limits of the arena
    viewer.cam.lookat[0] += 0.6        # x,y,z offset from the object (works if trackbodyid=-1)
    viewer.cam.lookat[1] += 0
    viewer.cam.lookat[2] += 1.0
    viewer.cam.elevation = -60           # camera rotation around the axis in the plane going through the frame origin (if 0 you just see a line)
    viewer.cam.azimuth = 180              # camera rotation around the camera's vertical axis


def main():
    """ Creates the environment and takes 200 time steps. MIMo takes no actions.
    The environment is rendered to an interactive window.
    """
    env = gym.make('MIMoMotionRetargeting-v0')
    env.seed(42)
    _ = env.reset()

    if not SAVE_MIMO:
        viewer = mjp.MjViewer(env.sim)
        viewer._render_every_frame = True
        viewer._advance_by_one_step = True
        viewer._paused = True
        viewer_setup(viewer)
        viewer.render()
    else:
        img = env.render(mode='rgb_array', width=1980, height=1080)
        viewer_setup(env)

    for i in range(24): # list geoms if needed
        geoms = utils.get_geoms_for_body(env.sim.model, i)

    for j in range(24): # list bodies if needed
        body_name = env.sim.model.body_id2name(j)

    ## LOAD THE ANGLES
    with open(f'{DATA}/arm_angles.csv', mode='r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        arm_angles = []
        header = next(csv_reader)
        for row in csv_reader:
            arm_angles.append(row)

    with open(f'{DATA}/leg_angles.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        leg_angles = []
        header = next(csv_reader)
        for row in csv_reader:
            leg_angles.append(row)

    with open(f'{DATA}/body_joint_angles.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        body_angles =[]
        header = next(csv_reader)
        for row in csv_reader:
            body_angles.append(row)
    
    ## INIT TOUCH PKL
    if SAVE_TOUCH:
        diction_touch = {'frames': len(body_angles)}


    for line_count in range(0, len(arm_angles)):
        idx = line_count//2
        arm_row = [np.float64(x) for x in arm_angles[line_count]]
        leg_row = [float(y) for y in leg_angles[line_count]]
        body_row = [float(z) for z in body_angles[line_count//2]]

        if line_count % 2 == 0:
            # set the left arm
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:left_shoulder_horizontal",  np.deg2rad(arm_row[0]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:left_shoulder_ad_ab", np.deg2rad(arm_row[1]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:left_shoulder_rotation", np.deg2rad(arm_row[2]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:left_elbow", -np.deg2rad(arm_row[3]))
            # set the left leg
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:left_hip1", -np.deg2rad(leg_row[0]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:left_hip2", -np.deg2rad(leg_row[1]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:left_hip3", np.deg2rad(leg_row[2])+np.pi/2)
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:left_knee", -np.deg2rad(leg_row[3]))
            # set the neck and torso 
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:hip_lean1", np.deg2rad(body_row[1]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:hip_rot1", np.deg2rad(body_row[2]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:hip_bend1", np.deg2rad(body_row[0]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:head_swivel", np.deg2rad(body_row[5]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:head_tilt", np.deg2rad(body_row[1]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:head_tilt_side", -np.deg2rad(body_row[4]))
        else:
            # set the right arm
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:right_shoulder_horizontal",  np.deg2rad(arm_row[0]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:right_shoulder_ad_ab", np.deg2rad(arm_row[1]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:right_shoulder_rotation", np.deg2rad(arm_row[2]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:right_elbow", -np.deg2rad(arm_row[3]))
            # set the right hip
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:right_hip1", -np.deg2rad(leg_row[0]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:right_hip2", -np.deg2rad(leg_row[1]))
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:right_hip3", np.deg2rad(leg_row[2])+np.pi/2)
            utils.set_joint_qpos(env.sim.model, env.sim.data, "robot:right_knee", -np.deg2rad(leg_row[3]))

            # forward the simulation
            env.sim.forward()
            if SAVE_MIMO:
                img = env.render(mode="rgb_array", width=1980, height=1080)
            else:   
                viewer.render()

            if SAVE_VISION:
                imgs_vision = env.vision.get_vision_obs()
            
            if SAVE_TOUCH:
                env.step(np.zeros(42))
                help = env.touch.get_contacts()
                touch_params = []
                for i in range(len(help)):
                    if np.max(help[i][2]) > 0:
                        print(f"{env.sim.model.body_id2name(help[i][1])} touches")
                        relat_pos = env.touch.get_contact_position_relative(help[i][0], help[i][1])
                        touch_params.append((help[i][0], help[i][1], help[i][2], relat_pos))
                diction_touch[idx] = touch_params
            
            if SAVE_MIMO:
                img = env.render(mode="rgb_array", width=1980, height=1080) # flush data
            if SAVE_MIMO:
                img = env.render(mode="rgb_array", width=1980, height=1080)
                img = Image.fromarray(img) # save data
                img.save(DATA + f'/mimo_bod/{idx:03}.png')
            else:
                viewer.render()
            #   #time.sleep(0.04)
            
            if SAVE_VISION:
                img_left = imgs_vision['eye_left']
                img_right = imgs_vision['eye_right']
                img = Image.fromarray(img_left)
                img.save(DATA + f'/eyes/left_{idx:03}.png')
                img = Image.fromarray(img_right)
                img.save(DATA + f'/eyes/right_{idx:03}.png')
            env.reset() # because of the step
            
    if SAVE_TOUCH:
        with open(f'{DATA}/touch_data.pkl', 'wb') as fp:
            pickle.dump(diction_touch, fp)
            print('dictionary saved to file')

    
def get_body_for_geom(env, geom):
    for i in range(24):
        geoms = utils.get_geoms_for_body(env.sim.model, i)
        if geom in geoms:
            return i, env.sim.model.body_id2name(i)
    return None, None

if __name__ == "__main__":
    main()
