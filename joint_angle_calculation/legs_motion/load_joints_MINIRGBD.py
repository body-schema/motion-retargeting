import numpy as np
import leg_joint_angles as lja

import sys
import copy
import csv
from os import listdir, mkdir, path
from os.path import join

global joints
joints = ['global_',
          'left_hip',
          'right_hip',
          'mid_hip',
          'left_knee',
          'right_knee',
          'mid_hip1',
          'left_ankle',
          'right_ankle',
          'mid_hip2',
          'left_toes',
          'right_toes',
          'neck',
          'left_shoulder',
          'right_shoulder',
          'head',
          'left_upper_arm',
          'right_upper_arm',
          'left_elbow',
          'right_elbow',
          'left_wrist',
          'right_wrist',
          'left_fingers',
          'right_fingers',
          'nose_vertex']

CROP = True

def load_joints(address):
    '''
    :param address: the adress of text file with joints
    :return: dictionary with xyz coordinates of the joint
    '''
    f = open(address, "r")
    result = {}
    for line in f:
        my_line = line.split()
        for i in range(4):
            if i < 3:
                my_line[i] = float(my_line[i])
            else:
                global joints
                result[joints[int(my_line[i])]] = np.array(my_line[0:3])
    return result

def numbers(x):
    '''
    function to determine key of sorting files of MINI_RGBD dataset
    :param x: the name of a file
    :return: digits acoording to which the sorting should be done
    '''
    return x[-9:-4]



def main():
    address = sys.argv[1]

    if not path.exists('data'):
        mkdir('data')

    if len(sys.argv) > 2 and sys.argv[2] == '0':
        f = open('data/leg_angles.csv', 'w', newline='')
    else:
        f = open('data/leg_angles.csv', 'a', newline='')

    writer1 = csv.writer(f)
    header = ['Hip pitch', 'Hip roll', 'Hip yaw', 'Knee angle',
              'Ankle pitch', 'Ankle roll']
    if sys.argv[2] == '1':
        writer1.writerow(header)

    ld = listdir(address)
    sorted_files = sorted(ld, key=numbers)

    old_an_la = [0 for i in range(16)]
    old_an_ra = [0 for i in range(16)]
    old_sp_la = [0 for i in range(16)]
    old_sp_ra = [0 for i in range(16)]

    for file in sorted_files:
        joints = load_joints(join(address, file))
        left_leg = lja.Leg(joints, True, CROP)
        row = left_leg.angles[0:len(header)]
        writer1.writerow(row)

        right_leg = lja.Leg(joints, False, CROP)
        row = right_leg.angles[0:len(header)]
        writer1.writerow(row)

    f.close()


if __name__ == '__main__':
    main()