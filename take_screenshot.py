import numpy as np
import pyautogui
import imutils
import cv2
import sys

# take a screenshot of the screen and store it in memory, then
# convert the PIL/Pillow image to an OpenCV compatible NumPy array
# and finally write the image to disk
etwas = int(sys.argv[1])
image = pyautogui.screenshot()
image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
name = "picture%05d.png" % etwas
cv2.imwrite(name, image)