function filtered = filter_data(data)

fs = 25;
fc = 0.071*fs - 0.00003*fs*fs;
n_order = 15;
[b,a] = butter(n_order, fc /(fs/2));
data_filt = filtfilt(b,a,data);


epsilon = sqrt( ...
    sum((data - data_filt).^2) * 100 ...
    / sum((data - mean(data)).^2)...
    );

fc2 = 0.06*fs - 0.000022*fs^2 + 5.95 * 1/epsilon;
[b,a] = butter(n_order, fc2 /(fs/2));
filtered = filtfilt(b,a,data);
end
