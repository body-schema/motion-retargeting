import numpy as np
import arms_motion.arm_joint_angles as aja
from numpy.linalg import norm
import arms_motion.load_joints_MINIRGBD as lj
from arms_motion.load_from_folder import numbers
from os import listdir, mkdir, path
from os.path import join
import csv

class BodyParams():
    def __init__(self) -> None:
        self.la_upper = 0
        self.la_lower = 0
        self.ra_upper = 0
        self.ra_lower = 0

class ChildPoseMapped():
    def __init__(self, r_elbow, l_elbow, r_wrist, l_wrist) -> None:
        self.r_elbow = r_elbow
        self.l_elbow = l_elbow
        self.r_wrist = r_wrist
        self.l_wrist = l_wrist



# remap the coordinates (in case of synthetic babies)
def remap_synthetic_to_robot(coords):
    result = np.array([coords[2], -coords[0], -coords[1]])

    return result

# extract baby body lengths from T-pose
def extract_body_length(joints):
    result = BodyParams()

    la = aja.Arm(joints, True)
    result.la_upper = -la.vec_up / norm(la.vec_up)
    result.la_upper = remap_synthetic_to_robot(result.la_upper)
    result.la_lower = -la.vec_fore / norm(la.vec_fore)
    result.la_lower = remap_synthetic_to_robot(result.la_lower)

    ra = aja.Arm(joints, False)
    result.ra_upper = -ra.vec_up / norm(ra.vec_up)
    result.ra_upper = remap_synthetic_to_robot(result.ra_upper)
    result.ra_lower = -ra.vec_fore / norm(ra.vec_fore)
    result.ra_lower = remap_synthetic_to_robot(result.ra_lower)

    return result


def map_arm_coords(params, shoulders):
    l_sh = shoulders[0]
    r_sh = shoulders[1]

    length_up = 152.28
    length_down = 137.3
    r_elbow = np.add(params.ra_upper * length_up, r_sh)
    l_elbow = np.add(params.la_upper * length_up, l_sh)

    r_wrist = np.add(params.ra_lower * length_down, r_elbow)
    l_wrist = np.add(params.la_lower * length_down, l_elbow)
    res = ChildPoseMapped(r_elbow, l_elbow, r_wrist, l_wrist)

    return res


def main():
    address = '/home/ondra/Plocha/project_iCub/icub-motion-retargeting/MINI-RGBD_web/12/joints_3D'
    ld = listdir(address)
    sorted_files = sorted(ld, key=numbers)

    f = open('coordinates_mapped.csv', 'a', newline='')
    writer = csv.writer(f)
    header1 = ["Shoulder", "", "", "Elbow", "", "", "Wrist", "", ""]
    header2 = ["x", "y", "z","x", "y", "z","x", "y", "z"]
    #writer.writerow(header1)
    #writer.writerow(header2)
    for file in sorted_files:

        joints = lj.load_joints(join(address, file))
        params = extract_body_length(joints)
        l_sh = np.array([-10.81659677, -110.1160779, 175.3])
        r_sh = np.array([-10.81659677,	110.1160779, 175.3])
        shoulders = [l_sh, r_sh]
        child = map_arm_coords(params, shoulders)
        left_arm = [l_sh.tolist(), child.l_elbow.tolist(), child.l_wrist.tolist()]
        la = [x for sublist in left_arm for x in sublist]
        right_arm = [r_sh.tolist(), child.r_elbow.tolist(), child.r_wrist.tolist()]
        ra = [y for sublist in right_arm for y in sublist]
        writer.writerow(la)
        writer.writerow(ra)
    print("Done.")



if __name__ == "__main__":
    main()