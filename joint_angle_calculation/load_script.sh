#!/bin/bash
# define the folder of canonical data
subfolder="/media/ondra/T7/A_Ondrej/TH/18w/TH_st_s_s18_18w-4d_supine"

# load the joint angles and save them into data/ directiory
python3 arms_motion/load_joints_CSV.py "$subfolder/canonical.csv" 0
python3 legs_motion/load_joints_CSV.py "$subfolder/canonical.csv" 0
python3 neck_n_torso_motion/load_joints_CSV.py "$subfolder/canonical.csv" 0