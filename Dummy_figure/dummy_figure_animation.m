clc, clear;
data = 'data_12w_s11'
%% LOAD JOINT ANGLES
legs = deg2rad(readmatrix(data+'/leg_angles.csv')); % define leg joint angles
arms = deg2rad(readmatrix(data+'/arm_angles.csv')); % define arm joint angles
torso = deg2rad(readmatrix(data+'/body_joint_angles.csv')); % define torso joint angles
face = torso(:, 4:6); % define head joint angle
torso = torso(:, 1:3);
hfig = figure;
%% CREATE FIGURE AND SHOW ROTATIONS (optional)
% figure; % create a figure
% membrane % plot the membrane
% hold on
tBox = annotation('textbox', [0, 0.05, 0.1, 0.1],...
    'LineStyle', 'none');
% create your text box and place it ...
% in about the same place as is automatically displayed
rotateObj = rotate3d(gcf);
rotateObj.ActionPreCallback = ...
    @(~,~)(set(tBox, 'String', []));...
    % don't want our text box when the automatic one is around
rotateObj.ActionPostCallback = ...
    @(~, event_obj)(set(tBox, 'String', ...
    ['Az: ', num2str(event_obj.Axes.View(1)), ...
    ' El: ',num2str(event_obj.Axes.View(2))]));...
    % populate when done rotating
rotate3d on
%% IMPORT ROBOT AND SHOW HIM IN A HOME POSITION
robot = importrobot("stickman.urdf");
config = homeConfiguration(robot);
% setShapeVisibility(robot, 'off')
plt = show(robot, config, 'FastUpdate', true, 'PreservePlot',false);
view(0, 77.8);
ylim([-5, 5]);
zlim([-5, 5]);
xlim([-5, 5]);
%% ITERATE DEFINED JOINT ANGLES -> MOVE THE ROBOT
maximum = min([size(face,1), size(torso,1), size(arms,1)/2, size(legs,1)/2]);
fps = 25;
for k=1:size(torso,1)
    config(1).JointPosition = legs(2*k-1,1); % left  hip   pitch
    config(2).JointPosition = legs(2*k-1,2); % left  hip   roll
    config(3).JointPosition = legs(2*k-1,3)+pi/2; % left  hip   yaw
    config(4).JointPosition = legs(2*k-1,4); % left  knee
    config(5).JointPosition = legs(2*k, 1); % right hip   pitch
    config(6).JointPosition = legs(2*k, 2); % right hip   roll
    config(7).JointPosition = legs(2*k, 3)+pi/2; % right hip   yaw
    config(8).JointPosition = legs(2*k, 4); % right knee
    config(9).JointPosition = torso(k,1); %       torso pitch
    config(10).JointPosition = torso(k,2); %      torso roll
    config(11).JointPosition = -torso(k,3); %      torso yaw
    config(12).JointPosition = face(k,1); %      head  pitch
    config(13).JointPosition = -face(k,2); %      head  roll
    config(14).JointPosition = face(k,3); %      head  yaw
    config(15).JointPosition = -arms(2*k-1,1); % left shoulder pitch
    config(16).JointPosition = arms(2*k-1,2); % left shoulder roll
    config(17).JointPosition =  -arms(2*k-1,3)+pi/2; % left shoulder yaw
    config(18).JointPosition = arms(2*k-1,4); % left elbow
    config(19).JointPosition = -arms(2*k, 1); % right shoulder pitch
    config(20).JointPosition = arms(2*k, 2); % right shoulder roll
    config(21).JointPosition = -arms(2*k, 3)+pi/2; % right shoulder yaw
    config(22).JointPosition = arms(2*k, 4); % right elbow
    plt = show(robot, config, 'FastUpdate', true, 'PreservePlot',false);
    pause(1/fps);
end

% Get the handle to the robot's visual object
visuals = findobj('Tag', robot);

% Disable the shadow by modifying the object's properties
for i = 1:numel(visuals)
    visuals(i).Visuals.ShadowEnable = 'off';
end
% show(robot, config)

%% SAVE THE FIGURE
% fname = 'frame1.pdf';
% picturewidth = 15; % set this parameter and keep it forever
% hw_ratio = 1.5; % feel free to play with this ratio
% set(findall(hfig,'-property','FontSize'),'FontSize',17) % adjust fontsize to your document
% 
% set(findall(hfig,'-property','Box'),'Box','off') % optional
% set(findall(hfig,'-property','Interpreter'),'Interpreter','latex') 
% set(findall(hfig,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex')
% set(hfig,'Units','centimeters','Position',[3 3 picturewidth hw_ratio*picturewidth])
% pos = get(hfig,'Position');
% set(hfig,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[pos(3), pos(4)])
% saveas(gcf,fname)