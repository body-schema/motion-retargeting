// -*- mode:C++; tab-width:4; c-basic-offset:4; indent-tabs-mode:nil -*-

#include <string>
#include <cstdio>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>

#include <yarp/os/Network.h>
#include <yarp/dev/ControlBoardInterfaces.h>
#include <yarp/dev/PolyDriver.h>
#include <yarp/os/Time.h>
#include <yarp/sig/Vector.h>

using namespace std;
using namespace yarp::dev;
using namespace yarp::sig;
using namespace yarp::os;


#define FILE_NUM 50
#define PERIOD 1/5
#define ACCELERATION 1000
#define SPEED 50


Property get_options(string body_part, string robot_name){
    string remotePorts = "/";
    remotePorts += robot_name;
    remotePorts += "/";
    remotePorts += body_part;

    string localPorts = "/test/";
    localPorts += body_part;

    Property options;
    options.put("device" , "remote_controlboard");
    options.put("local", localPorts);
    options.put("remote", remotePorts);

    return options;
}

int init_position_control(IPositionControl **pos, PolyDriver *driver, int *nj){;
    IEncoders *encs;
    bool ok;
    ok = driver->view(*pos) && driver->view(encs);
    if(!ok)
        return 1;
    (*pos)->getAxes(nj);
    Vector encoders, cmd;

    encoders.resize(*nj);
    cmd.resize(*nj);

    int i;
    for (i = 0; i < *nj; i++){
        cmd[i] = ACCELERATION;
    }
    (*pos)->setRefAccelerations(cmd.data());
    for(i =0; i<*nj; i++){
        cmd[i] = SPEED;
    }
    (*pos)->setRefSpeeds(cmd.data());

    printf("waiting for encoders");
    
    while(!encs->getEncoders(encoders.data()))
    {
        Time::delay(0.1);
        printf(".");
    }
    printf("\n");

    return 0;
}

int main(int argc, char *argv[]){
    // Load files with instructions
    fstream armAngles("data/arm_angles.csv");
    fstream legAngles("data/leg_angles.csv");
    fstream bodyAngles("data/body_joint_angles.csv");

    string line;

    Network yarp;
    Property params;
    params.fromCommand(argc, argv);

    if(!params.check("robot")){
        fprintf(stderr, "Please specify the name of the robot.\n");
        fprintf(stderr, "--robot name (e.g. icubSim)\n");
        return 1;
    }

    string robotName = params.find("robot").asString();

    int files;
    if(!params.check("frames_num")){
        fprintf(stderr, "Number of frames was not specifies, setting it to %d.\n", FILE_NUM);
        files = FILE_NUM;
    }else{
        files = params.find("frames_num").asInt32();
    }

    /* set arms */
    Property leftArmOpt = get_options("left_arm", robotName);
    PolyDriver leftArm(leftArmOpt);

    Property rightArmOpt = get_options("right_arm", robotName);
    PolyDriver rightArm(rightArmOpt);

    int njLA = 0;
    int njRA = 0;

    IPositionControl *posLeftArm, *posRightArm;

    int a = init_position_control(&posLeftArm, &leftArm, &njLA);
    int b = init_position_control(&posRightArm, &rightArm, &njRA);

    if(njLA != njRA || njLA == 0 || njRA == 0){
        fprintf(stderr, "[ERROR] SOMETHING WITH JOINTS IS OFF. \n");
        exit(101);
    }

    Vector commandRA, commandLA;
    commandLA.resize(njLA);
    commandRA.resize(njRA);

    /* set legs */
    Property leftLegOpt =  get_options("left_leg", robotName);
    PolyDriver leftLeg(leftLegOpt);

    Property rightLegOpt = get_options("right_leg", robotName);
    PolyDriver rightLeg(rightLegOpt);
    
    int njLL = 0;
    int njRL = 0;

    IPositionControl *posLeftLeg;
    IPositionControl *posRightLeg;

    a = init_position_control(&posLeftLeg, &leftLeg, &njLL);
    b = init_position_control(&posRightLeg, &rightLeg, &njRL);

    if(njLL != njRL || njLL == 0 || njRL == 0){
        fprintf(stderr, "[ERROR] SOMETHING WITH JOINTS IS OFF.\n");
        exit(101);
    }

    Vector commandRL, commandLL;
    commandRL.resize(njRL);
    commandLL.resize(njLL);

    /* set the rest of the body */
    Property torsoOpt = get_options("torso", robotName);
    PolyDriver torso(torsoOpt);

    Property neckOpt = get_options("head", robotName);
    PolyDriver neck(neckOpt);

    int njT = 0;
    int njN = 0;

    IPositionControl *posTorso;
    IPositionControl *posNeck;

    a = init_position_control(&posTorso, &torso, &njT);
    b = init_position_control(&posNeck, &neck, &njN);
    if(njT != 3 || njN != 6){
        fprintf(stderr, "[EROOR] Torso or neck joints not correctly loaded. \n");
        exit(101);
    }

    Vector commandT, commandN;
    commandT.resize(njT);
    commandN.resize(njN);

    /*start movement */
    getline(legAngles, line);
    getline(armAngles, line);
    getline(bodyAngles, line);


    for(int k = 0; k < files; k++){
        printf("[INFO]: We are starting a routine of replicating frame no. %d.\n", k);

        vector<float> arms;
        vector<float> legs;
        vector<float> body;

        for(int j = 0; j < 2; j++){
            getline(armAngles, line);
            string word;
            stringstream ss(line);
            while(getline(ss, word, ',')){
            arms.push_back(stof(word));
            }

            getline(legAngles, line);
            stringstream ss2(line);
            while(getline(ss2, word, ',')){
                legs.push_back(stof(word));
            }
            if(j == 0){
                getline(bodyAngles, line);
                stringstream ss3(line);
                while(getline(ss3, word, ',')){
                    body.push_back(stof(word));
                }
            }
        }

        int i = 0;
        for(i = 0; i < arms.size()/2; i++){
            commandLA[i] = arms[i];
            commandRA[i] = arms[i + arms.size()/2];
        }

        for(i = 0; i < legs.size()/2; i++){
            commandLL[i] = legs[i];
            commandRL[i] = legs[i + legs.size()/2];
        }
        for(i = 0; i < body.size()/3; i++){
            commandT[i] = body[i];
            commandN[i] = body[i + body.size()/3];
            commandN[i + body.size()/3] = body[i + 2*body.size()/3];
        }

        posRightArm->positionMove(commandRA.data());
        posLeftArm->positionMove(commandLA.data());
        posRightLeg->positionMove(commandRL.data());
        posLeftLeg->positionMove(commandLL.data());
        posNeck->positionMove(commandN.data());
        posTorso->positionMove(commandT.data());

        Time::delay(PERIOD);

        bool done = false;
        bool done_temp = false;

        while(!done){
            posRightArm->checkMotionDone(&done);
            posLeftArm->checkMotionDone(&done_temp);
            done = done && done_temp;
            posRightLeg->checkMotionDone(&done_temp);
            done = done && done_temp;
            posLeftLeg->checkMotionDone(&done_temp);
            done = done && done_temp;
            posNeck->checkMotionDone(&done_temp);
            done = done && done_temp;
            posTorso->checkMotionDone(&done_temp);
            done = done && done_temp;
            Time::delay(0.05);
        }
        printf("[INFO]: Finished proccesing frame no. %d.\n", k);
        // ! delete the comment if you want to see it frame by frame with pause
        Time::delay(1);

        // ! UNCOMMENT THIS SECTION TO TAKE A SCREENSHOT AFTER EACH MOTION FRAME
        /*
        char filename[50];
        int n;
        n =  sprintf(filename, "take_screenshot.py %d", k);
        std::string command = "python3 ";
        command += filename;
        system(command.c_str());
        */
    }
    //leftArm.close();
    //rightArm.close();
    //leftLeg.close();
    //rightLeg.close();
    printf("[INFO]: Everything done.\n");
}