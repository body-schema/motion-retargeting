# Retargeting Infant Movements to Baby Humanoid Robots

## Overview

This GitLab repository corresponds to the Retargeting Infant Movements to Baby Humanoid Robots thesis. It deals with the topic of transfering the movement of infants captured by camera into various robotic platforms including the iCub robot, MIMo simulator and Fetus simulator.


## Structure

The GitLab repository is structured as follows:
1. joint_angle calculation
    This folder contains the scripts for calculating the joint angles, together with the necessary math aparatus.
2. iCub_implementation
    This folder contains the files for motion retargeting onto the iCub robot simulator.
3. MIMo_implementation
    This folder contains the files for motion retargeting onto the MIMo simulator.
4. Fetus_implementation
    This folder contains the files for motion retargeting onto the Fetus simulator.
5. Dummy_figure
    This folder contains the files for animating the dummy figure with the joint angles.
6. dataset
    This folder contains the dataset for motion retargeting.
7. videos
    This folder contains the videos of the motion retargeting process.

## Joint angle calculation
To calculate the joint angles from file ```canonical.csv```, use the bash file ```load_script.sh```, modify the path to the file on the first rows on the file and then run it by comman
```bash
bash load_script.sh
```

## MIMo implementation
To succesfully run the MIMo implementation, you need to move the motion_retargeting.py script into the following structure:
```/path/to/MIMo/mimoEnv/motion_retargeting.py```. Then include the 'retargeting.xml' which contains the definition of the environment into
```/path/to/MIMo/mimoEnv/assets/retargeting.xml```. Additionaly, it is important to add to the file ```/path/to/MIMo/mimoEnv/__init__.py``` following lines:
```python
register(id='MIMoMotionRetargeting-v0',
        entry_point='mimoEnv.envs:MIMoRetargeting',
        max_episode_steps=6000, 
        )

register(id='MIMoMotionRetargeting-v1',
        entry_point='mimoEnv.envs:MIMoRetargeting2',
    max_episode_steps=500,
    )
```
And you are all set, just run the motion retargeting as
```bash
cd /path/to/MIMo/mimoEnv/
python3 motion_retargeting.py
```
## Fetus implementation
To succesfully run the Fetus implementation, you need to move the motion_retargeting.py script into the following structure:
```/path/to/fetussim/mujoco_infant_application/programs/motion_retargeting.py```
And you are all set, just run the motion retargeting as
```bash
cd /path/to/fetussim/mujoco_infant_application/
PYTHONPATH=. python3 motion_retargeting.py
```
## iCub implementation
We launch another terminal and launch the gazebo with iCub model.
```bash
gazebo b3m33hro-labs/lab3/tutorial_joint-interface/gazebo/worlds/robot_w_matress.sdf
```
The model is loaded with iCub_v2_5_fixed model, so it is important to have that one.
TWe create a folder ```icub``` and copy there the ```whole_body_motion.cpp``` and ```CMakeLists.txt```.
Then we build the ```whole_body_motion.cpp``` program as follows:
```bash
cd icub
mkdir build/
cd build/
cmake ..
make && make uninstall && make install
mv bin/whole_body_motion ..
cd ..
```
```whole_body_motion``` takes as argument with flag ```--frames_num``` the number of frames we want to replicate, as a second argument there should be ```--robot icubSim```, as we want to run our simulation on iCub Simulator.
```bash
./whole_body_motion --frames_num <number_of_frames> --robot icubSim
```
