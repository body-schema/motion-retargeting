from mujoco_infant.envs.isi_callback_env import ISICallbackEnv
from mujoco_infant.utils.abstract_logger import ISIDummyLogManager
import csv
import numpy as np
from mujoco_infant.envs.isi_mujoco.isi_calculate_ext_force import ISICalculateExtForce
from algorithm.spinal_cord_circuit.isi_util.isi_lowpass_daisie import ISILowpassDaISIE
import time
import pickle
import mujoco as mjp
import mujoco
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.pyplot as plt
from PIL import Image

initial_joint_values = {
        "baby1_pelvis_tilt": np.pi/2,
        "baby1_pelvis_list": -np.pi/2,
        "baby1_pelvis_rotation": 0,
        "baby1_pelvis_tx": -0.2,
        "baby1_pelvis_ty": -0.1,
        "baby1_pelvis_tz": 0.0,
    }

SAVE_IMAGE = True
SAVE_TOUCH = False
SAVE_VISION = True
DATA = "data"
MODEL_PATH = "230126_04_isi_infant_floor_mocap.xml"


### DEACTIVATION OF VISIBILITY OF THE TACTILE SENSORS ###
# get colormap
cmap_name = "GnBu"
ncolors = 1024
color_array = plt.get_cmap(cmap_name)(range(ncolors))

# change alpha values
color_array[:, -1] = np.linspace(0, 1, ncolors)

# create a colormap object
cmap = LinearSegmentedColormap.from_list(name=cmap_name, colors=color_array)

def get_site_rgba(value):
    # Blue
    # ret = [0, 0, 1, value]

    # Red
    # ret = [1, 0, 0, value]

    # Cmap
    ret = cmap(value)
    return ret


def load_data_header(file_name):
    ret = []
    with open(file_name, "r") as f:
        reader = csv.reader(f, delimiter="\t")
        for header in reader:
            ret.extend(header)
    return ret


def main():
    ## init touch
    touch = []
    ## init external force
    ext_force = ISICalculateExtForce(
    contact_geom_groups=[
        ISICallbackEnv.GEOM_GROUP_DEFAULT,
        ISICallbackEnv.GEOM_GROUP_RIGID_SKIN,
        ISICallbackEnv.GEOM_GROUP_SOFT_SKIN,
        ISICallbackEnv.GEOM_GROUP_ENV,
    ],
    use_spread_collision_sensing = True,
    use_hydro_force=False,
    use_centripetal_force=False,
    centripetal_radius=0.1,
    )
    ## init environment
    env=ISICallbackEnv(model_path=MODEL_PATH, 
                       ext_force=ext_force, 
                       initial_visible_joint_value=initial_joint_values)
    
    ## if not save touch -- deactivate the visibility of sensors 
    if not SAVE_TOUCH:
        tactile_names  = load_data_header(f"tactile_names.txt")
        valid_tactile_ids = []
        valid_only_display_site_ids = []
        for tactile_id, tactile_name in enumerate(tactile_names):
            baby_name = tactile_name.split("_")[0]
            only_display_site_name = f"{baby_name}_only_display_{tactile_name}"

            if only_display_site_name in env.site_names:
                only_display_site_id = mujoco.mj_name2id(
                    env.model, mujoco.mjtObj.mjOBJ_SITE, only_display_site_name
                )
                valid_tactile_ids.append(tactile_id)
                valid_only_display_site_ids.append(only_display_site_id)

        # Change the color of tactile site
        for only_display_site_id in valid_only_display_site_ids:
            env.model.site_size[only_display_site_id][0] = 0
            env.model.site_size[only_display_site_id][1] = 0
            env.model.site_size[only_display_site_id][2] = 0


    ## set the names for joints
    left_arm_joints, right_arm_joints, \
        left_leg_joints, right_leg_joints,rt, \
            torso, neck= set_joint_names(env)
    obs = env.reset()
    rgb = []

    # init viewers
    if SAVE_IMAGE:
        rgb = []
        rgb_array = env.render(  
            mode="rgb_array",
            camera_id=0,
            render_width=1920,
            render_height=1080)
    else:
        env.render()
        env.renderer._paused = True

    if SAVE_VISION:
        lefteye =[]
        righteye=[]    

    action = env.sample_action

    ## load joints
    arm_angles = load_csv(f"{DATA}/arm_angles.csv")
    leg_angles = load_csv(f"{DATA}/leg_angles.csv")
    other_angles = load_csv(f"{DATA}/body_joint_angles.csv")

    # begin the process of motion retargeting
    for line_count in range(0, len(arm_angles)):
        # modify according to positive and negative directions
        arm_row = [np.deg2rad(np.float64(x)) for x in arm_angles[line_count]]
        arm_row[2] += arm_row[0]
        leg_row = [np.deg2rad(np.float64(x)) for x in leg_angles[line_count]]
        leg_row[1] = -leg_row[1] # roll
        leg_row[2] = -leg_row[2] - np.pi/2
        env.data.qvel[:]=0
        env.data.qacc[:]=0

        if line_count % 2 == 0: # left
            # modify torso and neck as well
            other_row = [np.deg2rad(np.float64(x)) for x in other_angles[line_count//2]]
            other_row[3] *= -1
            other_row[4] *= -1

            # LEFT ARM
            for i in range(len(left_arm_joints)):
                env.set_visible_joint_value(
                    left_arm_joints[i],
                    arm_row[i],
                    forward=False,
                )
                action[i+26] = arm_row[i]
            
            # RIGHT ARM
            for i in range(len(left_leg_joints)):
                env.set_visible_joint_value(
                    left_leg_joints[i],
                    leg_row[i],
                    forward=False,
                )
                action[i+6] = leg_row[i]

            # TORSO
            for i in range(3):
                env.set_visible_joint_value(
                    torso[i],
                    other_row[i],
                    forward=False,
                )
            action[i+12] = other_row[0]
            action[i+13] = other_row[1]
            action[i+14] = other_row[2]
            
            # HEAD
            for i in range(3):
                env.set_visible_joint_value(
                    neck[i],
                    other_row[i+3],
                    forward=False,
                )
            action[i+18] = other_row[3]#pitch roll yaw
            action[i+19] = other_row[4]
            action[i+20] = other_row[5]
        else:
            ## RIGHT ARM
            for i in range(len(right_arm_joints)):
                env.set_visible_joint_value(
                    right_arm_joints[i],
                    arm_row[i],
                    forward=False,
                )
                action[i+21] = arm_row[i]

            ## RIGHT LEG
            for i in range(len(right_leg_joints)):
                env.set_visible_joint_value(
                    right_leg_joints[i],
                    leg_row[i],
                    forward=False,
                )
                action[i] = leg_row[i]

            env.forward(force_load_visible_value=True, force_modify_pos=True)

            if SAVE_IMAGE:
                for i in range(3): ## flushing
                    rgb_array = env.render(
                    mode="rgb_array",
                    camera_id=0,
                    render_width=1920,
                    render_height=1080,
                )
                rgb.append(rgb_array)

                """direct save
                img = Image.fromarray(rgb_array)
                idx = line_count // 2
                img.save(DATA + f'/{idx:05}.png'
                """
            else:
                env.render()

            if SAVE_VISION:
                for i in range(3):
                    left_eye = env.render(
                    mode="rgb_array",
                    camera_id=2,
                    render_width=1920,
                    render_height=1080,
                )
                """ direct save
                img = Image.fromarray(left_eye)
                idx = line_count // 2
                img.save(DATA + f'/left_eye{idx:05}.png')"""

                lefteye.append(left_eye)
                for i in range(3):
                    right_eye = env.render(
                    mode="rgb_array",
                    camera_id=1,
                    render_width=1920,
                    render_height=1080,
                )
                """ direct save
                img = Image.fromarray(right_eye)
                idx = line_count // 2
                img.save(DATA + f'/right_eye{idx:05}.png')"""
                righteye.append(right_eye)

            if SAVE_TOUCH:
                env.step((action))
                env.ext_force.update()
                tactile = env.ext_force.get_tactile_pressure()
                touch.append(tactile)
            print(line_count //2)
            env.reset()

    if SAVE_TOUCH:
        with(open(f'{DATA}/touch_data.pkl', 'wb')) as fp:
            pickle.dump(touch, fp)
            print('dictionary of touch saved to file')

    if SAVE_IMAGE:
        with(open(f'{DATA}/rgb_image_data.pkl', 'wb')) as  fp:
            pickle.dump(rgb, fp)
            print('dictionary of rgb saved to file')

    if SAVE_VISION:
        with(open(f'{DATA}/left_eye_data.pkl', 'wb')) as  fp:
            pickle.dump(lefteye, fp)
            print('dictionary of left eye saved to file')
        with(open('right_eye_data.pkl', 'wb')) as  fp:
            pickle.dump(righteye, fp)
            print('dictionary of right eye saved to file')


def load_csv(filename):
    with open(filename, mode='r') as csv_file:
        csv_reader = csv.reader(csv_file,delimiter=',')
        angles = []
        header=next(csv_reader)
        for row in csv_reader:
            angles.append(row)
    return angles

def set_joint_names(env):
    joints = env.get_visible_joint_names()
    rt = joints[0:6]
    torso = joints[18:21]
    neck=joints[24:27]
    right_leg = joints[6:10]
    left_leg = joints[12:16]
    right_arm = joints[27:32]
    left_arm = joints[32:37]
    return left_arm, right_arm, left_leg, right_leg, rt, torso, neck

if __name__ == "__main__":
    main()
